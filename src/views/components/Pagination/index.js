import React from 'react';
import { Pagination as CustomPagination } from 'react-bootstrap';

const Pagination = ({totalPost,postPerPage,paginate}) => {

    const pageNumber=[];

    for(let i = 1; i<=Math.ceil(totalPost / postPerPage); i++){
        pageNumber.push(
            <CustomPagination.Item onClick={()=>paginate(i)}>
                {i}
            </CustomPagination.Item>
        )
    }
    return ( 
        <CustomPagination>{pageNumber}</CustomPagination>
    );
}
 
export default Pagination;