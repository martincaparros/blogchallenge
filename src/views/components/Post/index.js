import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Image } from 'react-bootstrap';
import axios from 'axios';

const Post = ({id,title}) => {

    const {push} = useHistory();
    
    const handleOnDelete = async ( )=>{
        const deletedPost= await axios.delete(` https://jsonplaceholder.typicode.com/posts/${id}`);
        console.log(deletedPost);
    }

    return ( 
        <div className="border">
            <div>
                <div className="d-flex  justify-content-center">
                    <Image src="https://via.placeholder.com/300" fluid />
                </div>
                <p className="text-center bold">{title}</p>
            </div>
            <div className="d-flex justify-content-center">
                <Button size="sm" className="m-1 btn-primary" onClick={()=>push(`/post/${id}`)}>Details</Button>
                <Button size="sm" className="m-1 btn-success" onClick={()=>push(`/post-edit/${id}`)}>Edit</Button>
                <Button size="sm" className="m-1 btn-danger" onClick={()=>handleOnDelete()}>Delete</Button>
            </div>
        </div>
    );
}
 
export default Post;