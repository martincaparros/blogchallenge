import React,{ useState,useEffect } from 'react';
import  axios  from 'axios';
import { Post, Pagination } from '../../components';
import { Spinner,Row,Col } from 'react-bootstrap';

const PostsGroup = () => {

    const [allPost,setAllPost]= useState([]);
    const [loading,setLoading] = useState(false);
    const [currentPage,setCurrentPage] = useState(1);
    const [postPerPage] = useState(10);

    useEffect(()=>{
        const fetchPosts = async () => {
            try{
                setLoading(true)
                const response = await axios.get('https://jsonplaceholder.typicode.com/posts') 
                setAllPost(response.data)
            }catch(error){
                console.log(error)
            }finally {
                setLoading(false)
            }
        };
        fetchPosts();
    },[]);

    const indexLastPost = currentPage * postPerPage;
    const indexFirstPost = indexLastPost - postPerPage;
    const currentPost = allPost.slice(indexFirstPost,indexLastPost)

    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber)
    }  


    if (loading) {
        return (
            <div className="d-flex justify-content-center mt-5">
                <Spinner animation="border" variant="info"/>
            </div>
        )
    }else {
        return(
            <>
                <Row className="m-0 justify-content-md-center">
                    {currentPost.map(({id,title,body}) => {

                        return(
                            <Col
                                key={id}
                                md={2}
                            >
                                <Post
                                    id={id}
                                    title={title}
                                />
                            </Col>
                        )
                    })}
                </Row>
                <div className="d-flex justify-content-center">
                    <Pagination
                        postPerPage={postPerPage}
                        totalPost={allPost.length}
                        paginate={paginate}
                    />
                </div>
            </>
        )
    };
};
 
export default PostsGroup;