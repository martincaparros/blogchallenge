import React,{ useEffect,useState } from 'react';
import axios from 'axios';
import { useParams,useHistory } from 'react-router-dom';
import { Spinner,Button } from 'react-bootstrap';

const PostDetailContainer = () => {

    const [detailedPost,setDetailedPost] = useState({})
    const [loading,setLoading] = useState(false)
    const [notFound,setNotFound]= useState(false)

    const {id}=useParams();
    const {push}=useHistory();

    useEffect(() => {
        const getDetailedPost = async () => {
            try{
                setNotFound(false)
                setLoading(true);
                const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
                setDetailedPost(response.data);
            }catch(error){
                setNotFound(true)
                console.log(error);
            }finally {
                setLoading(false)
            }
        
        }
        getDetailedPost()
    }, [id])

    if(loading){
        return(
            <div className="d-flex justify-content-center mt-5">
                <Spinner animation="border" variant="info"/>
            </div>
        )
    }else if(notFound){
        return(
            <div className="d-flex flex-column align-items-center">
                <div className="mt-3 d-flex flex-column justify-content-center">
                    <h3 className="text-center">Sorry we couldn't found your the post you were looking for</h3>
                </div>
                <Button onClick={()=>push("/home")}>Click me</Button>
            </div>
        )
    }else {
        return (
            <div className="d-flex justify-content-center">
                <div className="border d-flex flex-column justify-contet-center w-25">
                    <h4>Title: {detailedPost.title}</h4>
                    <span className="fs-3">Information: </span>
                    <p>{detailedPost.body}</p>
                </div>
            </div>
        )
    }

}
 
export default PostDetailContainer;