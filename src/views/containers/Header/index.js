import React from 'react';
import { useHistory } from 'react-router';
import { Navbar,Nav,Container } from 'react-bootstrap';
import { CustomIcon,CustomLink } from './styled';
import { faBook } from '@fortawesome/free-solid-svg-icons';

const Header = () => {

    const {push}=useHistory()

    return(
        <Navbar className="mb-2" bg="primary" variant="dark" expand="lg">
            <Container className="m-0">
                <CustomIcon icon={faBook} size="2x" onClick={()=>push("/home")}/>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav>
                        <CustomLink className="ms-3 fs-5" href="" onClick={()=>push("/new-post")}>New Topic</CustomLink >
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}


export default Header