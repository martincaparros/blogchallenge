import React,{ useState } from 'react';
import { Formik } from 'formik';
import { FloatingLabel,Form,Button,Container } from 'react-bootstrap';
import axios from 'axios';

const PostCreationPanel = () => {
    
    
    const [error, setError]=useState('')

    return(
        <div className="d-flex justify-content-center mt-5">
            <Formik
                initialValues={{
                    title:'',
                    content:''
                }}
                onSubmit = { async () => {
                    try {
                        setError('')
                        await axios.post('https://jsonplaceholder.typicode.com/posts')
                    } catch(error) {
                        setError('Wrong password, please try again')
                    } finally {
                    }
                }}
            >
                {({
                    values,
                    isSubmitting,
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    
                })=> (
                    <Container className="p-5">
                        <h5 className="mb-5 text-center">Welcome to your personal Blog!</h5>
                        <form onSubmit={handleSubmit}>
                            <FloatingLabel
                                controlId="floatingTitle"
                                label="Title"
                                className="mb-3"
                            >
                                <Form.Control 
                                    type="text"
                                    name="title"
                                    placeholder="Title"
                                    value={values.title} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}

                                />
                            </FloatingLabel>
                            <FloatingLabel controlId="floatingPassword" label="Content">
                                <Form.Control 
                                    name="content" 
                                    type="content" 
                                    placeholder="Content" 
                                    value={values.content} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </FloatingLabel>
                            {error && ( <p>Something seems wrong, please try again</p> )}
                            <div className=" d-flex justify-content-center">
                                <Button type="submit" disabled={isSubmitting} className="mt-2">Post</Button>
                            </div>
                        </form>

                    </Container>

                )
                }
            </Formik>
        </div>
    )
}
 
export default PostCreationPanel;