import React from 'react';
import { Switch,Redirect } from 'react-router';
import { PrivateRoute } from '../../components';
import { Header } from '..';
import { PostsGroup,PostDetailContainer,PostEditingPanel,PostCreationPanel } from '../../containers';
import { Content } from './styled';

const Home = () => {

    return ( 
        <>
            <Header/>
            <Content>
                <Switch>
                    <PrivateRoute
                        exact={true}
                        path="/home"
                        component={PostsGroup}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/post/:id"
                        component={PostDetailContainer}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/post-edit/:id"
                        component={PostEditingPanel}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/new-post"
                        component={PostCreationPanel}
                    />
                    <Redirect to="/login" />
                </Switch>
            </Content>
        </>


    );
}
 
export default Home;