import React,{ useState, useEffect} from 'react';
import { useParams } from 'react-router';
import axios from 'axios';
import { Spinner,Container,FloatingLabel,Form } from 'react-bootstrap';
import { PostDetailContainer } from '../../containers'
import { useFormik } from 'formik';

const PostEditingPanel = () => {

    const {id}=useParams()
    const [postToEdit, setPostToEdit] = useState({});
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const getPostToEdit = async () => {
            try{
                setLoading(true);
                const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
                setPostToEdit(response.data);
            }catch(error){
                console.log(error);
            }finally {
                setLoading(false)
            }
        
        }
        getPostToEdit()
    }, [id])


    const formik = useFormik({
        initialValues: {
          title: ``,
          body: ``,
        },
        onSubmit: async () => {
            await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`);
        },
    });

    if(loading){
        return(
            <div className="d-flex justify-content-center mt-5">
                <Spinner animation="border" variant="info"/>
            </div>
        )
    }else {
        return (
            <div>
                <PostDetailContainer></PostDetailContainer>
                <Container>
                    <form onSubmit={formik.handleSubmit}>
                        <FloatingLabel 
                            controlId="postTitle"
                            label="New Title"
                            className="mb-3"
                        >
                            <Form.Control
                                name="postTitle"
                                type="text"
                                onChange={formik.handleChange}
                            />
                        </FloatingLabel>
                    
                        <FloatingLabel 
                            controlId="postBody"
                            label="New content"
                            className="mb-3"
                        
                        >
                            <Form.Control
                                name="postBody"
                                type="text"
                                onChange={formik.handleChange}
                            />      
                        </FloatingLabel>  
                        <button type="submit">Guardar</button>
                    </form>
                </Container>
            </div>
        );
    };
}

 
export default PostEditingPanel;