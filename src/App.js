import React from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom'
import {Home, Login} from './views/containers'
import { PrivateRoute } from './views/components';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login"  component ={Login}/>
        <PrivateRoute
          path="/"
          component={Home}
        />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
